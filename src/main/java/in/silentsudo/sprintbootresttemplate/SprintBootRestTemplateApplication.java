package in.silentsudo.sprintbootresttemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintBootRestTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SprintBootRestTemplateApplication.class, args);
    }

}
