package in.silentsudo.sprintbootresttemplate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/users")
public class UsersApis {

    @GetMapping(value = "/profile")
    public Map<String, Object> profile() {
        return Map.of("profile",
                Map.of("name", "silentsudo",
                        "active", true,
                        "email", "ungabunga@gmail.com")
        );
    }

    @GetMapping(value = "/address")
    public Map<String, Object> address() {
        return Map.of("address",
                Map.of("state", "KA",
                        "country", "India",
                        "zipcode", "000 000")
        );
    }
}
