package in.silentsudo.sprintbootresttemplate.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(2)
@Slf4j
public class ComposeApiAroundAspect {

    @Around("execution(* in.silentsudo.sprintbootresttemplate.ApiCompositionController.simple(..))")
    public Object simpleExecuteBeforeAndAfterCompose(ProceedingJoinPoint point) throws Throwable {
        log.debug("Memory before execution {}", (Runtime.getRuntime().freeMemory() / (1024 * 1024)));
        final Object result = point.proceed();
        log.debug("Memory After execution {} MB", (Runtime.getRuntime().freeMemory() / (1024 * 1024)));
        return result;
    }

    @Around("execution(* in.silentsudo.sprintbootresttemplate.ApiCompositionController.parallel(..))")
    public Object parallelExecuteBeforeAndAfterCompose(ProceedingJoinPoint point) throws Throwable {
        log.debug("Memory before execution {}", (Runtime.getRuntime().freeMemory() / (1024 * 1024)));
        final Object result = point.proceed();
        log.debug("Memory After execution {} MB", (Runtime.getRuntime().freeMemory() / (1024 * 1024)));
        return result;
    }

}
