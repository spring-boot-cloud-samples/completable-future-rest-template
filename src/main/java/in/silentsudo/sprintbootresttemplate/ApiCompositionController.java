package in.silentsudo.sprintbootresttemplate;

import io.micrometer.core.annotation.Timed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Supplier;

@RestController
@RequestMapping(value = "/compose")
@AllArgsConstructor
public class ApiCompositionController {
    private final RestTemplate restTemplate;
//    private final ExecutorService executorService = Executors.newFixedThreadPool(2048, r -> {
//        final Thread thread = new Thread(r);
//        thread.setDaemon(true);
//        thread.setName("Exe-Thread: " + thread.getId() + " - " + thread.getThreadGroup());
//        return thread;
//    });

    final ThreadPoolExecutor executorService = new ThreadPoolExecutor(1024, 2048,
            5, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(),
            r -> {
                final Thread thread = new Thread(r);
                thread.setDaemon(true);
                thread.setName("Exe-Thread: " + thread.getId() + " - " + thread.getThreadGroup());
                return thread;
            });

    {
        executorService.allowCoreThreadTimeOut(true);
    }

    @GetMapping(value = "/cores")
    public Map<String, String> cores() {
        return Map.of("cores", "" + Runtime.getRuntime().availableProcessors());
    }

    @Timed
    @GetMapping(value = "/simple")
    public Map<String, Object> simple() {
        return Map.of("status", "simple",
                "profile", new UserProfileSupplier(restTemplate).get().map(UserProfileResponse::getProfile).orElse(new UserProfile()),
                "address", new UserAddressSupplier(restTemplate).get().map(UserAddressResponse::getAddress).orElse(new UserAddress())
        );
    }

    @Timed
    @GetMapping(value = "/parallel")
    public CompletableFuture<Map<String, Object>> parallel() throws ExecutionException, InterruptedException {
        CompletableFuture<Optional<UserProfileResponse>> upFuture = CompletableFuture
                .supplyAsync(new UserProfileSupplier(restTemplate), executorService);
        CompletableFuture<Optional<UserAddressResponse>> uaFuture = CompletableFuture
                .supplyAsync(new UserAddressSupplier(restTemplate), executorService);
        return upFuture.thenCombine(uaFuture, (userProfileResponse, userAddressResponse)
                -> Map.of("status", "parallel",
                "profile", userProfileResponse.map(UserProfileResponse::getProfile).orElse(new UserProfile()),
                "address", userAddressResponse.map(UserAddressResponse::getAddress).orElse(new UserAddress())
        ));
    }

    @Data
    public static class UserProfileResponse {
        private UserProfile profile;
    }

    @Data
    public static class UserProfile {
        private String name;
        private String email;
        private boolean active;
    }

    @Data
    public static class UserAddressResponse {
        private UserAddress address;
    }

    @Data
    public static class UserAddress {
        private String state;
        private String zipcode;
        private String country;
    }

    @Slf4j
    @AllArgsConstructor
    public static class UserProfileSupplier implements Supplier<Optional<UserProfileResponse>> {
        private final RestTemplate restTemplate;


        @SneakyThrows
        @Override
        public Optional<UserProfileResponse> get() {
            log.debug("Executing UserProfile in: " + Thread.currentThread().getName());
            Thread.sleep(1000L);
            final ResponseEntity<UserProfileResponse> response = restTemplate
                    .getForEntity("http://localhost:8080/users/profile", UserProfileResponse.class);
            if (response.hasBody()) {
                return Optional.ofNullable(response.getBody());
            }
            return Optional.empty();
        }
    }

    @Slf4j
    @AllArgsConstructor
    public static class UserAddressSupplier implements Supplier<Optional<UserAddressResponse>> {
        private final RestTemplate restTemplate;


        @SneakyThrows
        @Override
        public Optional<UserAddressResponse> get() {
            log.debug("Executing UserAddress in: " + Thread.currentThread().getName());
            Thread.sleep(1000L);
            final ResponseEntity<UserAddressResponse> response = restTemplate
                    .getForEntity("http://localhost:8080/users/address", UserAddressResponse.class);
            if (response.hasBody()) {
                return Optional.ofNullable(response.getBody());
            }
            return Optional.empty();
        }
    }
}
